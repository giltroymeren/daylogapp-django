from django.conf.urls import url, include

from . import views

app_name = 'static'
urlpatterns = [
    url(r'^$', views.get_index, name='get_index'),
]
