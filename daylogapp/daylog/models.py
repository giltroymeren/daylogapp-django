from django.db import models

class Daylog(models.Model):
    CATEGORY_ADEQUATE = "Adequate"
    CATEGORY_MINOR = "Minor"
    CATEGORY_MAJOR = "Major"
    CATEGORY_CHOICES = (
        (CATEGORY_ADEQUATE, CATEGORY_ADEQUATE),
        (CATEGORY_MINOR, CATEGORY_MINOR),
        (CATEGORY_MAJOR, CATEGORY_MAJOR),
    )

    title = models.CharField(max_length=255, blank=False)
    slug = models.CharField(max_length=50, blank=False, unique=True)
    location = models.CharField(max_length=100, blank=False)
    log_at = models.DateField(blank=False, unique=True)
    category = models.CharField(max_length=25, choices=CATEGORY_CHOICES, blank=False)

    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "%s: %s" % (self.log_at, self.title)
