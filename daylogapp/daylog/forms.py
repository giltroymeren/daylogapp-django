from django import forms

from .models import Daylog

class DaylogForm(forms.ModelForm):
    class Meta:
        model = Daylog
        fields = "__all__"

    def __init__(self, *args, **kwargs):
        self.operation = kwargs.pop('operation', None)
        super(DaylogForm, self).__init__(*args, **kwargs)

    def full_clean(self):
        super(DaylogForm, self).full_clean()

        if self.operation == 'edit':
            if 'slug' in self._errors:
                del self._errors['slug']
            if 'log_at' in self._errors:
                del self._errors['log_at']

        return self.cleaned_data