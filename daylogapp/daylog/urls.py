from django.conf.urls import url, include

from . import views

app_name = 'daylog'
urlpatterns = [
    url(r'^$', views.get_list, name='get_list'),
    url(r'^create/$', views.create, name='create'),
    url(r'^(?P<slug>.+)/delete$', views.delete, name='delete'),
    url(r'^(?P<slug>.+)/edit$', views.edit, name='edit'),
    url(r'^(?P<slug>.+)/$', views.details, name='details'),
]
