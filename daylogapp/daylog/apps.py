from django.apps import AppConfig


class DaylogConfig(AppConfig):
    name = 'daylog'
