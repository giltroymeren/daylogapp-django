from rest_framework import serializers

from .models import Daylog

class DaylogSerializer(serializer.ModelSerializer):

    class Meta:
        model = Daylog
        fields = ('id', 'title', 'slug', 'location', 'log_at', 'category')
        read_only_fields = ('created_at', 'modified_at')
