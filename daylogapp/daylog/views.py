from django.contrib import messages
from django.http import Http404
from django.shortcuts import redirect, render

from .models import Daylog
from .forms import DaylogForm

# TODO: Add proper loggers

def get_list(request):
    daylogs = Daylog.objects.all()
    return render(request, 'daylog/index.html', { 'daylogs': daylogs })

def details(request, slug):
    try:
        daylog = Daylog.objects.get(slug = slug)
    except Daylog.DoesNotExist:
        # TODO: Put error message in a constant file, maybe in model?
        messages.error(request, "Day Log does not exist.", "danger")
        return get_list(request)

    return render(request, 'daylog/form.html', {
        'daylog': daylog,
        'operation': 'view'
    })

def create(request):
    if request.method == 'POST':
        # TODO: Add user-daylog authentication mmodified_atdleware
        daylog_form = DaylogForm(request.POST)
        if daylog_form.is_valid():
            daylog_form.save()
            messages.info(request, "Day Log created.", "info")
            return redirect('daylog:get_list')
        else:
            return render(request, 'daylog/form.html', {
                'daylog': request.POST,
                'errors': daylog_form.errors
             })

    return render(request, 'daylog/form.html', {
        'action': 'daylog:create',
        'operation': 'create',
    })

def delete(request, slug):
    # TODO: Add user-daylog authentication middleware
    try:
        daylog = Daylog.objects.get(slug = slug)
    except Daylog.DoesNotExist:
        messages.error(request, "Day Log does not exist.", "danger")
        return redirect('daylog:get_list')

    daylog.delete()
    messages.error(request, "Day Log deleted.", "info")
    return redirect('daylog:get_list')

def edit(request, slug):
    try:
        old_daylog = Daylog.objects.get(slug = slug)

        if request.method == 'POST':
            # TODO: Add user-daylog authentication middleware
            daylog_form = DaylogForm(request.POST, operation='edit')
            if daylog_form.is_valid():
                new_daylog = daylog_form.instance
                new_daylog.id = old_daylog.id
                new_daylog.created_at = old_daylog.created_at
                new_daylog.modified_at = old_daylog.modified_at
                new_daylog.save()
                messages.info(request, "Day Log edited.", "info")
                return redirect('daylog:get_list')
            else:
                return render(request, 'daylog/form.html', {
                    'daylog': request.POST,
                    'action': 'daylog:edit',
                    'operation': 'edit',
                    'errors': daylog_form.errors
                 })
    except Daylog.DoesNotExist:
        messages.error(request, "Day Log does not exist.", "danger")
        return redirect('daylog:get_list')

    return render(request, 'daylog/form.html', {
        'daylog': old_daylog,
        'action': 'daylog:edit',
        'operation': 'edit',
    })
