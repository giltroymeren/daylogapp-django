from django.core.urlresolvers import reverse
from django.test import Client, TestCase

from rest_framework import status

from .models import Daylog
from .forms import DaylogForm

def get_daylog_data():
    return {
        "title": "Lorem ipsum dolor sit amet",
        "slug": "lorem-ipsum",
        "location": "ABC XYZ-112233",
        "log_at": "2017-01-01",
        "category": Daylog.CATEGORY_ADEQUATE,
    }

def create_daylog():
    daylog = get_daylog_data()
    return Daylog.objects.create(
        title = daylog["title"],
        slug = daylog["slug"],
        location = daylog["location"],
        log_at = daylog["log_at"],
        category = daylog["category"],
    )

class ModelTests(TestCase):
    def test_model_can_create_a_daylog(self):
        old_count = Daylog.objects.count()
        create_daylog().save()
        new_count = Daylog.objects.count()

        self.assertTrue(old_count < new_count)

class ViewTests(TestCase):
    def test_index_with_no_daylogs(self):
        response = self.client.get(reverse('daylog:get_list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, "There are no Day Logs available.")
        self.assertQuerysetEqual(response.context['daylogs'], [])

    def test_index_with_a_daylog(self):
        create_daylog()
        response = self.client.get(reverse('daylog:get_list'))

        self.assertQuerysetEqual(
            response.context['daylogs'],
            ['<Daylog: 2017-01-01: Lorem ipsum dolor sit amet>']
        )

    def test_detail_with_a_valid_daylog(self):
        daylog = create_daylog()
        response = self.client.get(reverse('daylog:details', args=(daylog.slug,)))

        self.assertEqual(response.context['daylog'], daylog)
        self.assertEqual(response.context['operation'], 'view')

    def test_detail_with_an_invalid_daylog(self):
        response = self.client.get(reverse('daylog:details', args=('fake-slug',)))

        for message in response.context['messages']:
            self.assertEqual(message.message, "Day Log does not exist.")
            self.assertEqual(message.extra_tags, "danger")
            break;

    def test_create_view(self):
        response = self.client.get(reverse('daylog:create'))

        self.assertEqual(response.context['action'], 'daylog:create')
        self.assertEqual(response.context['operation'], 'create')

    def test_create_and_submit_with_valid_daylog(self):
        test_daylog = get_daylog_data()
        response = self.client.post(reverse('daylog:create'), test_daylog, follow=True)

        db_daylog = Daylog.objects.get(slug = test_daylog['slug'])

        self.assertEqual(test_daylog['title'], db_daylog.title)
        self.assertEqual(test_daylog['slug'], db_daylog.slug)
        self.assertEqual(test_daylog['location'], db_daylog.location)
        self.assertEqual(test_daylog['log_at'], db_daylog.log_at.strftime('%Y-%m-%d'))
        self.assertEqual(test_daylog['category'], db_daylog.category)

        for message in response.context['messages']:
            self.assertEqual(message.message, "Day Log created.")
            self.assertEqual(message.extra_tags, "info")
            break;

    def test_create_and_submit_with_empty_daylog(self):
        response = self.client.post(reverse('daylog:create'), {})

        self.assertTrue("title" in response.context['errors'])
        self.assertTrue("slug" in response.context['errors'])
        self.assertTrue("location" in response.context['errors'])
        self.assertTrue("log_at" in response.context['errors'])
        self.assertTrue("category" in response.context['errors'])

    def test_create_and_submit_with_existing_slug_and_log_at(self):
        create_daylog()

        test_daylog = get_daylog_data()
        response = self.client.post(reverse('daylog:create'), test_daylog)

        self.assertTrue("slug" in response.context['errors'])
        self.assertTrue("log_at" in response.context['errors'])

    def test_delete_existing_daylog(self):
        daylog = create_daylog()
        daylog.save()

        self.assertTrue(Daylog.objects.filter(slug = daylog.slug).count())

        response = self.client.post(reverse('daylog:delete', args=(daylog.slug,)), follow=True)

        self.assertFalse(Daylog.objects.filter(slug = daylog.slug).count())

        for message in response.context['messages']:
            self.assertEqual(message.message, "Day Log deleted.")
            self.assertEqual(message.extra_tags, "info")
            break;

    def test_delete_nonexistent_daylog(self):
        nonexistent_slug = "non-existent-slug"

        self.assertFalse(Daylog.objects.filter(slug = nonexistent_slug).count())

        response = self.client.post(reverse('daylog:delete', args=(nonexistent_slug,)), follow=True)

        for message in response.context['messages']:
            self.assertEqual(message.message, "Day Log does not exist.")
            self.assertEqual(message.extra_tags, "danger")
            break;

    def test_edit_view(self):
        daylog = create_daylog()
        daylog.save()

        self.assertTrue(Daylog.objects.filter(slug = daylog.slug).count())

        response = self.client.get(reverse('daylog:edit', args=(daylog.slug,)))

        self.assertEqual(response.context['action'], 'daylog:edit')
        self.assertEqual(response.context['operation'], 'edit')

    def test_edit_existing_daylog_without_changes(self):
        old_daylog = create_daylog()
        old_daylog.save()

        self.assertTrue(Daylog.objects.filter(slug = old_daylog.slug).count())

        new_daylog = old_daylog.__dict__

        response = self.client.post(reverse('daylog:edit', args=(old_daylog.slug,)),
            new_daylog, follow=True)

        for message in response.context['messages']:
            self.assertEqual(message.message, "Day Log edited.")
            self.assertEqual(message.extra_tags, "info")
            break;

    def test_edit_existing_daylog_with_valid_changes(self):
        old_daylog = create_daylog()
        old_daylog.save()

        self.assertTrue(Daylog.objects.filter(slug = old_daylog.slug).count())

        new_daylog = old_daylog.__dict__
        new_daylog['title'] = "New title"

        response = self.client.post(reverse('daylog:edit', args=(old_daylog.slug,)),
            new_daylog, follow=True)

        for message in response.context['messages']:
            self.assertEqual(message.message, "Day Log edited.")
            self.assertEqual(message.extra_tags, "info")
            break;
